persons = [
    {
        'name': 'player',
        'health': 100,
        'damage': 25,
        'armor': 1.2
    },
    {
        'name': 'enemy',
        'health': 100,
        'damage': 25,
        'armor': 1.2
    }
]

for person in persons:
    person['name'] = input('Please input name for ' + person['name'] + ': ')


def attack(person1, person2):
    def damage_part():
        global new_damage
        new_damage = person1['damage'] / person2['armor']
        person2['armor'] = 0

    damage_part()
    person2['health'] -= new_damage


attack(persons[0], persons[1])
# print(persons[0])
# print(persons[1])
for person in persons:
    for key in person:
        print(key, ':', person[key])
