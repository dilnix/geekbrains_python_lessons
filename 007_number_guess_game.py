import random

number = random.randint(1, 100)

user_num = None
count = 0
levels = {1: 10, 2: 6, 3: 3}

level = int(input('Choose your complexity level (from 1 to 3): '))
max_count = levels[level]

user_count = int(input('Please type a user amount to play: '))
users = []
for i in range(user_count):
    user_name = input(f'Please type a name for user {i}: ')
    users.append(user_name)

print(users)

is_winner = False
winner_name = None
while not is_winner:
    count += 1
    if count > max_count:
        print('All users are lose because used all possible tries!')
        break
    print(f'Your try # {count}')
    for user in users:
        print(f'User {user} try')
        user_num = int(input('Please insert number: '))
        if user_num == number:
            is_winner = True
            winner_name = user
            break
        elif number < user_num:
            print('No, it\'s smaller one')
        else:
            print('No, it\'s greater one')
else:
    print(f'User {winner_name} is winner!')
