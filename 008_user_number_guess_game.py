least, biggest = 0, 101
guessed = len(list(range(least, biggest))) // 2

winner = None
ask = input('Please think a number from 1 to 100 and write it down somewhere.\nAre you ready? (Please submit "Y" or "y"): ')

if ask.lower() == 'y':
    while winner != 'yes':
        print(f'Is this your number - {guessed}?')
        winner = input(
            'Please answer with "greater", "smaller" or "yes": ').lower()
        if winner == 'greater':
            least = guessed
            guessed += (biggest - least) // 2
        elif winner == 'smaller':
            biggest = guessed
            guessed -= (biggest - least) // 2
        elif winner == 'yes':
            continue
        else:
            print('You typed something wrong, please try again.')
    else:
        print('Finally, I guessed your number ' + str(guessed) + '!')
else:
    print('Guess you not going to play with me, ByBy!')
