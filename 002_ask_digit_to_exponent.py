# ask a digit from 1 to 9 to exponentiate it
while True:
    number = int(input('Please type a digit not equal 0: '))
    if number > 0 and number < 10:
        print('You digit multiplied by itself equals:', number ** 2)
        break
    else:
        print('Incorrect digit! I need one from 1 to 9.')
