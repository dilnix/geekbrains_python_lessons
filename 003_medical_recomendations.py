# Medical Recomendations
firstName = input('Please type your first name: ')
lastName = input('Please type your last name: ')
age = int(input('Please type your age (complete years of age): '))
weight = float(input('Please type you weight in KG (possible with floating point): '))

if age <= 30:
    if (40 <= weight < 90):
        status = 'good'
        print(firstName, lastName + ',', age, 'years old,', 'weights', weight, 'kg,', 'health status -', status)
    else:
        status = 'neglected, you need to take care of yourself!'
        print(firstName, lastName + ',', age, 'years old,', 'weights', weight, 'kg,', 'health status -', status)
elif age <= 40:
    if (50 <= weight < 120):
        status = 'good'
        print(firstName, lastName + ',', age, 'years old,', 'weights', weight, 'kg,', 'health status -', status)
    else:
        status = 'neglected, you need to take care of yourself!'
        print(firstName, lastName + ',', age, 'years old,', 'weights', weight, 'kg,', 'health status -', status)
else:
    if (50 <= weight < 120):
        status = 'good'
        print(firstName, lastName + ',', age, 'years old,', 'weights', weight, 'kg,', 'health status -', status)
    else:
        status = 'terrible, you need to see a doctor!!!'
        print(firstName, lastName + ',', age, 'years old,', 'weights', weight, 'kg,', 'health status -', status)
