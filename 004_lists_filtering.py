my_list_1 = [2, 5, 8, 2, 12, 12, 4]
my_list_2 = [2, 7, 12, 3]

my_list_1 = list(set(my_list_1) - set(my_list_2))
print(my_list_1)

# method from author
my_list_1 = [2, 5, 8, 2, 12, 12, 4]
my_list_2 = [2, 7, 12, 3]

for number in my_list_1[:]:
    if number in my_list_2:
        my_list_1.remove(number)
